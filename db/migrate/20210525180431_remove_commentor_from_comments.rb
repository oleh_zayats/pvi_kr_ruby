class RemoveCommentorFromComments < ActiveRecord::Migration[6.1]
  def change
    remove_column :comments, :commenter
    add_reference :comments, :author, null: false, foreign_key: { to_table: :users }
  end
end
