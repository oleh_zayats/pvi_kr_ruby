class Article < ApplicationRecord
    include Visible
    belongs_to :author, class_name: 'User'
    has_one_attached :image
    has_many :comments, dependent: :destroy
  
    validates :title, presence: true
    validates :body, presence: true, length: { minimum: 10 }
    validates :image, presence: true

    paginates_per 3

  end
  