class ArticlesController < ApplicationController

   before_action :authenticate_user!, only: %i[ new create edit update destroy]
  
  def index
    authorize Article
    @articles = Article.order(created_at: :desc).includes(:author).page params[:page]
  end

  def show
    @article = Article.find(params[:id])
    authorize @article
  end

  def new
    @article = Article.new
    authorize Article
  end

  def create
    authorize Article
    @article = Article.new(article_params)

    @article.author = current_user

    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    @article = Article.find(params[:id])
    authorize @article
  end

  def update
    
    @article = Article.find(params[:id])
    authorize @article

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    @article = Article.find(params[:id])
    authorize @article
    @article.destroy

    redirect_to root_path
  end

  def search
    if params[:search].blank?
      redirect_to root_path
    else
      @parameter = params[:search].downcase
      @articles = Article.all.where("lower(body) LIKE :search OR lower(title) LIKE :search", search: "%#{@parameter}%")
    end
  end




    private
    def article_params
      params.require(:article).permit(:title, :body, :status, :image)
    end

end