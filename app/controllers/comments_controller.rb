class CommentsController < ApplicationController
  before_action :authenticate_user!, only: %i[ create destroy]
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params.merge(:author_id => current_user.id))
    @comment.author = current_user
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    authorize @comment
    @comment.destroy
    redirect_to article_path(@article)
  end

  private
    def comment_params
      params.require(:comment).permit(:body, :status)
    end
end

