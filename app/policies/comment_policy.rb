class CommentPolicy < ApplicationPolicy

  def destroy?
    author? || admin?
  end


  private
  def author?
    return false unless user

    record.author_id == user.id
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
